# System Security Permissions

## What
* System.Security.Permissions 4.6.0 [net461]

## Installation

```bash
"com.yenmoc.system-security-permissions":"https://gitlab.com/yenmoc/system-security-permissions"
or
npm publish --registry=http://localhost:4873
```
